#!/bin/bash

#!/bin/bash
#==============================================================================

: <<COMMENTBLOCK

Function:   Deface
Purpose:    deface the T1w image to deidentify the data for
            data sharing.
Input:      T1w image in ${bids_dir}/anat
Output:     A defaced T1w image in ${bids_dir}/anat and the defacing masks
            The original T1w image is moved to sourcedir/${subj}/anat

COMMENTBLOCK

Deface ()
{
echo "------------------------------------------------"
if [ ! -d ${bids_dir}/sourcedata/${subj}/anat ]; then
  mkdir -p ${bids_dir}/sourcedata/${subj}/anat
fi

# If the original has not been copied to sourcedata, then copy and reorient it.
if [ ! -e ${bids_dir}/sourcedata/${subj}/anat/${img_base} ]; then
  echo "Copy the original image for safe keeping"
  cp ${bids_dir}/${subj}/anat/${img_base} ${bids_dir}/sourcedata/${subj}/anat/
fi

if [ ! -e ${bids_dir}/${subj}/anat/${img_base_stem}_defaced.nii.gz ]; then
  echo "reorienteing ${img_base_stem}"
  fslreorient2std ${bids_dir}/${subj}/anat/${img_base_stem} ${bids_dir}/${subj}/anat/${img_base_stem}
  echo "defacing ${img_base}"
  mri_deface ${bids_dir}/${subj}/anat/${img_base_stem}.nii ${SHARED}/talairach_mixed_with_skull.gca ${SHARED}/face.gca ${bids_dir}/${subj}/anat/${img_base_stem}_defaced.nii
  gzip ${bids_dir}/${subj}/anat/${img_base_stem}_defaced.nii
  fslmaths ${bids_dir}/${subj}/anat/${img_base_stem}_defaced.nii.gz ${bids_dir}/${subj}/anat/${img_base_stem}_defaced.nii -odt short
  fslmaths ${SCIF_APPDATA}/${img_stem}_defaced -bin ${SCIF_APPDATA}/${img_stem}_defacemask -odt char
fi
echo "=================================================================="
echo "You should check your defacing results.  The mask should cover the entire brain:"
echo ">fsleyes ${img_stem} ${img_stem}_defacemask -cm Red -a 40"
echo "If the mask does not entirely overlap the brain, manually add voxels, then,"
echo ">fslmaths ${img_stem} -mul ${img_stem}_defacemask ${img_stem}_defaced"

}

#==============================================================================
: <<COMMENTBLOCK
See Deface
COMMENTBLOCK

# We pass 3 arguments from deface_bids.py: the subjectnum, bids_dir and output_dir

subjectnum=$1
subj=sub-${subjectnum}
bids_dir=$2
output_dir=$3
img=`ls ${bids_dir}/${subj}/anat/${subj}*T1*.nii.gz`
img_base=`basename ${img}`
img_base_stem=`remove_ext ${img_base}`
###################
echo "subject=${subj}"
echo "bids_dir = ${bids_dir}"
echo "output_dir = ${output_dir}"

if [ $# -lt 1 ]
    then
        HelpMessage
        exit 1
fi

Deface ${subjectnum} ${bids_dir} ${output_dir}
#==============================================================================
