This repository contains a set of tools implemented as SCIF recipes in an FSL 5.X container.  You can read about SCIF here:
Sochat, V. (2018). The Scientific Filesystem (SCIF). GigaScience, 7(5), 1–17. http://doi.org/10.1093/gigascience/giy023/4931737

# Examples

Get the scif usage message:   
`docker run -ti --rm diannepat/scif_fsl`  

Learn about what apps are in the container:    
`docker run -ti --rm diannepat/scif_fsl apps`

Get a bash shell in the container:   
`docker run -ti --rm diannepat/scif_fsl shell`

Request help for a particular app (roixtractor):   
`docker run -ti --rm diannepat/scif_fsl app roixtractor`

Inspect the scif recipe used in the container for a particular app:   
`docker run -ti --rm diannepat/scif_fsl dump roixtractor`

Download sfw.sh (short for scif_fsl_wrap) and put it in your path to facilitate running some of these docker SCIF commands.

# Apps

## deface_fs
  deface_fs is an app based on Freesurfer's mri_deface. It removes identifiable
  features from a T1w image.  This uses Freesurfer's deface tool, and incorporates
  necessary helper files, so that all the user has to do is to select and specify a
  T1w image to process.  FSL 5.x tools are used to orient the image etc. A mask is
  generated of the region that has been removed, so the user can inspect, and if
  necessary, modify the results.
  The following docker command that should work (assuming the T1w image exists):

  `docker run -it --rm -v ${PWD}:/scif/data/deface diannepat/scif_fsl run deface sub-0162_anat_T1w.nii.gz`

  This scif is designed to be installed on the BIDS base FSL 5.X package: https://hub.docker.com/r/bids/base_fsl/. See the example Dockerfile: https://bitbucket.org/dpat/scif_fsl/src/master/Dockerfile

---

## roixtractor
  roixtractor is an app for extracting descriptive statistics for each region in a standard MNI 2mm image. The results are useful for choropleth-type visualizations, explained here: https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/choropleths.html. The git repository https://bitbucket.org/dpat/scif_fsl/src/master/ includes all the atlas ROIS. You can get a test image run-01_IC-06_Russian_Unlearnable.nii here: https://osf.io/uty3v/

  To run roixtractor you must pass it:
  1) The function to run: `Stats`, `Lat` or `Mask`.    
    **Stats**: generates mean and peak values for above threshold voxels in a stats image.   
    **Lat**: generates laterality info for above threshold values in a stats image.   
    **Mask**: will generate percent_masked values for each region.  It works with a binary mask in standard space.   
  2) A statistical image (or binary mask) in 2mm FSL MNI standard space 
     If it is not in that space, but is MNI, it will be backed up to a *_orig file and then the main image will be converted to the proper space.
  3) A statistical threshold (output regions must be equal to or greater than this value).
  4) An available atlas: `HO-CB`, `HCP-MMP1, `ARTERIAL1`, `ARTERIAL2`, `ARTERIAL1_WM-GM`, `Schaefer2018_100Parcels_Kong2022_17Networks`, `Schaefer2018_100Parcels_17Networks`.
  
----

  - **HO-CB**: The Harvard-Oxford cortical and subcortical atlases combined with a cerebellar atlas.
  - **HCP-MMP1**: Although it is possible to output these values in HCP-MMP1 space, it is probably not a
  good idea to use the HCP-MMP1 atlas in volumetric space.  It is not likely to be terribly
  accurate).
  The git repository https://bitbucket.org/dpat/scif_fsl/src/master/ includes all the atlas ROIS.
  - **Arterial_Territories_Level2** comes from https://www.nitrc.org/projects/arterialatlas.  
  See Liu C-F, Hsu J, Xu X, Kim G, Sheppard SM, Meier EL et al. (2021) Digital 3D Brain MRI Arterial Territories Atlas. BioRxiv 
  - **Schaefer atlases** are available from the git repository: https://github.com/ThomasYeoLab/CBIG/tree/master/stable_projects/brain_parcellation/Schaefer2018_LocalGlobal
  See Schaefer A, Kong R, Gordon EM, Laumann TO, Zuo XN, Holmes AJ, Eickhoff SB, Yeo BTT. Local-Global parcellation of the human cerebral cortex from intrinsic functional connectivity MRI. Cerebral Cortex, 29:3095-3114, 2018.
  
----

### Stats    

If you run **Stats**:     
  - roixtractor will generate CSV files and NIfTI image containing the regions from the chosen atlas that meet the threshold:  
    - the CSV (e.g., HO-CB_run-01_IC-06_Russian_Unlearnable_1.3_stats.csv) will contain the fields:  
      Region, Mean, Max, SD, LR, Lobe, CogX, CogY, CogZ, RegionLongName, RegionVol_MM3, Image.  
    - For example:   
  BA44_R,1.664311,2.824052,0.597232,R,Fr,19,71,44,Inferior_Frontal_Gyrus_pars_opercularis_R,5240,run-01_IC-06_Russian_Unlearnable_1.5
  
  ----

  The returned columns (illustrated above) include the following:    
  
  1) **Region**: the short region name;
  2) **Mean**, **Max** and **SD** are descriptive statistics for each region.
  3) **LR**: identifies the region as left or right (redundant, but useful for sorting)
  4) **Lobe**: The lobe in which that region is found
  5) **CogX**, **CogY** and **CogZ** are the x,y,z center of gravity voxel coordinates for each region
  6) **RegionLongName**: A more detailed name for the region if available
  7) **RegionVol_MM3**: The volume of the region in cubic mm
  8) **Image**: The name of the image you passed to the program: the name of the atlas you used is
  prepended and the threshold you used is appended to the name.  Because image is the last field,
  you can easily use Excel to split the data on underscores. This splitting is useful if you want
  to concatenate several CSVs from different runs of the xtractor.
  
----

  A NIfTI image will be generated that fills each such region with the mean value:
  e.g., HO-CB_run-01_IC-06_Russian_Unlearnable_1.3_stats.nii.gz

  ----

  Using Docker, here's a sample run of the Stats function:
  docker run -it --rm -v ${PWD}:/scif/data/roixtractor diannepat/scif_fsl run roixtractor Stats run-01_IC-06_Russian_Unlearnable.nii 1.3 HO-CB
  
----

  This assumes the container is called diannepat/scif_fsl and that you are in a directory with the sample file run-01*.
  This scif is designed to be installed on the BIDS base FSL 5.X package: https://hub.docker.com/r/bids/base_fsl/
  See https://bitbucket.org/dpat/scif_fsl/raw/master/Dockerfile.base which adds scif tools to FSL5.
 
----

### Lat   

  If you run Lat:      
  The CSV file will contain laterality information based on Wegrzyn, M., Mertens, M., Bien,
  C. G., Woermann, F. G., & Labudda, K. (2019). Quantifying the Confidence in fMRI-Based Language
  Lateralisation Through Laterality Index Deconstruction. Frontiers in Neurology, 10, 106915.
  http://doi.org/10.3389/fneur.2019.00655
  
----

  The number of Suprathreshold voxels in each region is reported for left and right homologs.  
  Strength=L+R;  
  Laterality=L-R;  
  LI=laterality/strength  
  
----
### Mask

Mask with threshold=0 provides volume of each ROI that is lesion (cubed mm and percentage).
This assumes a binary 1-0 mask file in standard space.

----

# Wrapper Script

 - Use the wrapper script sfw.sh to simplify the commands.  Here are some examples:

  `sfw.sh xtract Stats run-01_IC-06_Russian_Unlearnable.nii 1.3 HO-CB`
  
  `sfw.sh xtract Lat run-01_IC-06_Russian_Unlearnable.nii 1.3 HO-CB`

  In this example, we process a binary mask. Note the threshold is set to 0.  
  We use the HCP-MMP1 atlas for illustration, but you could use HO-CB instead:

  `sfw.sh xtract Mask w1158lesion.nii.gz 0 HCP-MMP1`


- Download [sfw.sh](https://bitbucket.org/dpat/scif_fsl/raw/master/sfw.sh) bash script to some location in your path. sfw.sh wraps the scif docker commands:

- Displays general scif help and a list of the apps in the container
`sfw.sh`

- If you provide one argument, it can be `shell`, `pyshell`, `version`, e.g.,
`sfw.sh version`

- If you provide two arguments, they should be an action followed by the name of an app, e.g.,
`sfw.sh help roixtractor`

- If you provide **xtract** as your first argument, then you can run roixtractor by providing: 
1) the function (Stats, Lat or Mask),   
2) input image (usually a stats image for the Stats or Lat functions, and a binary 1-0 mask for the Mask function),   
3) stats threshold, and   
4) atlas (HO-CB or HCP-MMP1)  

**Stats** will generate a CSV and NIfTI image containing the above threshold regions (i.e., where the mean is greater than or equal to the chosen threshold) from the chosen atlas. For each such above threshold region, the CSV will contain the mean and the max values. The NIfTI image will contain the mean value in each region. e.g.,

`sfw.sh xtract Stats run-01_IC-06_Russian_Unlearnable.nii 1.3 HO-CB`

If you run **Lat**, the CSV file will contain laterality information based on Wegrzyn, M., Mertens, M., Bien,C. G., Woermann, F. G., & Labudda, K. (2019). Quantifying the Confidence in fMRI-Based Language Lateralisation Through Laterality Index Deconstruction. Frontiers in Neurology, 10, 106915. http://doi.org/10.3389/fneur.2019.00655


The number of Suprathreshold voxels in each region is reported for left and right homologs.

- Strength=L+R; 
- Laterality=L-R;  
- LI=laterality/strength   

`sfw.sh xtract Lat run-01_IC-06_Russian_Unlearnable.nii 1.3 HO-CB`

- If you run **Mask**, you should set the threshold to 0. This will provide the volume of each ROI that is lesion (as cubed mm and as a percentage)
`sfw.sh xtract Mask w1158lesion.nii.gz 0 HO-CB`

- If you provide **deface** as your first argument, then you can run Freesurfer defacing.
by providing the T1w NIfTI image to be defaced e.g.,
`sfw.sh deface anat_T1w`
