
# =======================
# deface_fs
# =======================

%appinstall deface_fs

%appenv deface_fs

%applabels deface_fs
  MAINTAINER Dianne Patterson
  VERSION 1.1

%apphelp deface_fs
  deface_fs is an app based on Freesurfer's mri_deface. It removes identifiable features from a T1w
  image.  This uses Freesurfer's deface tool, and incorporates necessary helper files, so that all
  the user has to do is to select and specify a T1w image to process.  FSL 5.x tools are used to
  orient the image etc. A mask is generated of the region that has been removed, so the user can
  inspect, and if necessary, modify the results.
  The following docker command that should work (assuming the T1w image exists):
  -------------------------------------------------------------------------------------------------
  docker run -it --rm -v ${PWD}:/scif/data/deface_fs scif_fsl run deface_fs sub-0162_anat_T1w.nii.gz
  -------------------------------------------------------------------------------------------------
  This scif is designed to be installed on the BIDS base FSL 5.X package: https://hub.docker.com/r/bids/base_fsl/. See the example Dockerfile: https://bitbucket.org/dpat/scif_fsl/src/master/Dockerfile

%apprun deface_fs
  exec deface_fs_scif.sh $1
