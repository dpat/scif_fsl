#!/bin/bash

: <<COMMENTBLOCK

Function: CheckDim
Purpose: 	Make sure the dimensions are correct: 2mm FSL MNI (91x109x91)
          Possible Issues: 
          It is in 2mm SPM space: 90 108 90
          It is in 1mm SPM space: 181 217 181
          It is in 1mm FSL space: 182 218 182
TO-DO: -odt should be int16 unless it is a mask and should be char          

COMMENTBLOCK

CheckDim() {
  # Retrieve image dimensions
  DIM1=$(fslinfo ${img_loc} | grep -m 1 dim1 | awk '{print $2}')
  DIM2=$(fslinfo ${img_loc} | grep -m 1 dim2 | awk '{print $2}')
  DIM3=$(fslinfo ${img_loc} | grep -m 1 dim3 | awk '{print $2}')

  # Fix image dimensions if any of the three dimensions fail to meet the criteria.
  if [[ ${DIM1} -ne 91 || ${DIM2} -ne 109 || ${DIM3} -ne 91 ]]; then
    echo "${img_stem} is ${DIM1}x${DIM2}x${DIM3}, not the required 91x109x91"
    if [ -f ${img_loc}.nii ]; then
      echo "The image is not zipped, zipping it now so we don't get duplicates."
      gzip ${img_loc}.nii
    fi
    echo "Backing up original image as ${img_stem}_orig.nii.gz and resampling this image to 91x109x91"
    imcp ${img_loc} ${img_loc}_orig
    flirt -in ${img_loc} -ref ${SHARED}/MNI152_T1_2mm.nii.gz -applyxfm -usesqform -noresampblur -interp nearestneighbour -out ${img_loc}
  fi
}
#==============================================================================
: <<COMMENTBLOCK

Function: CleanLat
Purpose: 	Define each measure of interest only once

COMMENTBLOCK

CleanLat() {
  # Remove the files if they already exist in the current working directory for the Lat function
  for file in ${OUTFILE_LI_L} ${OUTFILE_LI_R} ${OUTFILE_LI_TEMP} ${OUTFILE_LI}; do
    if [ -f ${file} ]; then
      echo "${file} exists, removing and replacing"
      rm ${file}
    fi
  done
}
#==============================================================================
: <<COMMENTBLOCK

Function: CleanMask
Purpose: 	Define each measure of interest only once

COMMENTBLOCK

CleanMask() {
  # Remove the files if they already exist in the current working directory for the Stats function
  for file in ${OUTFILE_MASK1} ${OUTFILE_MASK2}; do
    if [ -f ${file} ]; then
      echo "${file} exists, removing and replacing"
      rm ${file}
    fi
  done
}

#==============================================================================

: <<COMMENTBLOCK

Function: CleanStats
Purpose: 	Define each measure of interest only once

COMMENTBLOCK

CleanStats() {
  # Remove the files if they already exist in the current working directory for the Stats function
  for file in ${OUTFILE1} ${OUTFILE2} ${OUTIMG}; do
    if [ -f ${file} ]; then
      echo "${file} exists, removing and replacing"
      rm ${file}
    fi
  done
}
#==============================================================================
: <<COMMENTBLOCK

Function: DefMeasuresLat
Purpose: 	Define each measure of interest only once

COMMENTBLOCK

DefMeasuresLat() {
  roi_base=$(basename -s .nii.gz ${roi})
  roi_base_LI_L=$(basename -s _L.nii.gz ${roi})
  roi_base_LI_R=$(basename -s _R.nii.gz ${roi})
  # Calculate the mean, SD and Peak values in each region from Atlas/roi*
  MeanStat=$(fslstats ${img_loc} -k ${roi} -M 2>/dev/null)
  SDStat=$(fslstats ${img_loc} -k ${roi} -S 2>/dev/null)
  PeakStat=$(fslstats ${img_loc} -k ${roi} -R | awk '{print $2}')
  PeakX=$(fslstats ${img_loc} -k ${roi} -x | awk '{print $1}')
  PeakY=$(fslstats ${img_loc} -k ${roi} -x | awk '{print $2}')
  PeakZ=$(fslstats ${img_loc} -k ${roi} -x | awk '{print $3}')
  VoxSuprathresh_L=$(fslstats ${img_loc} -k ${roi} -l ${stat_thresh} -V | awk '{print $2}' | sed -e 's/\.[0-9]*//')
  VoxSuprathresh_R=$(fslstats ${img_loc} -k ${roi} -l ${stat_thresh} -V | awk '{print $2}' | sed -e 's/\.[0-9]*//')
}
#==============================================================================
: <<COMMENTBLOCK

Function: DefMeasuresMask
Purpose: 	Define each measure of interest only once

COMMENTBLOCK

DefMeasuresMask() {
  roi_base=$(basename -s .nii.gz ${roi})
  RoiVol=$(fslstats ${roi} -V | awk '{print $2}' | sed -e 's/\.[0-9]*//')
  # Calculate the number of suprathreshold voxels in the ROI
  MaskVol=$(fslstats ${img_loc} -k ${roi} -l ${stat_thresh} -V | awk '{print $2}' | sed -e 's/\.[0-9]*//')
  percent_mask=$(echo "scale=6; (${MaskVol} / ${RoiVol}) * 100" | bc)
}

#==============================================================================
: <<COMMENTBLOCK

Function: DefMeasuresStats
Purpose: 	Define each measure of interest only once

COMMENTBLOCK

DefMeasuresStats() {
  roi_base=$(basename -s .nii.gz ${roi})
  # Calculate the mean, SD and Peak values in each region from Atlas/roi*
  MeanStat=$(fslstats ${img_loc} -k ${roi} -M 2>/dev/null)
  SDStat=$(fslstats ${img_loc} -k ${roi} -S 2>/dev/null)
  PeakStat=$(fslstats ${img_loc} -k ${roi} -R | awk '{print $2}')
  PeakX=$(fslstats ${img_loc} -k ${roi} -x | awk '{print $1}')
  PeakY=$(fslstats ${img_loc} -k ${roi} -x | awk '{print $2}')
  PeakZ=$(fslstats ${img_loc} -k ${roi} -x | awk '{print $3}')
}
#==============================================================================
: <<COMMENTBLOCK
Function: DefVarsLat
Purpose: 	Define each variable of interest only once
COMMENTBLOCK

DefVarsLat() {
  OUTFILE_LI_L=${SCIF_APPDATA}/${atlas}_${img_stem}_${stat_thresh}_LI_L.csv
  OUTFILE_LI_R=${SCIF_APPDATA}/${atlas}_${img_stem}_${stat_thresh}_LI_R.csv
  OUTFILE_LI_TEMP=${SCIF_APPDATA}/${atlas}_${img_stem}_${stat_thresh}_LI_temp.csv
  OUTFILE_LI=${SCIF_APPDATA}/${atlas}_${img_stem}_${stat_thresh}_LI.csv
}
#==============================================================================
: <<COMMENTBLOCK
Function: DefVarsMask
Purpose: 	Define each variable of interest only once for the Lat function
COMMENTBLOCK

DefVarsMask() {
  OUTFILE_MASK1=${SCIF_APPDATA}/ROIValues_Mask.csv
  OUTFILE_MASK2=${SCIF_APPDATA}/${atlas}_${img_stem}_mask.csv
}

#==============================================================================
: <<COMMENTBLOCK
Function: DefVarsStats
Purpose: 	Define each variable of interest only once for the Stats function
COMMENTBLOCK

DefVarsStats() {
  OUTFILE1=${SCIF_APPDATA}/ROIValues.csv
  OUTFILE2=${SCIF_APPDATA}/${atlas}_${img_stem}_${stat_thresh}_stats.csv
  OUTIMG=${SCIF_APPDATA}/${atlas}_${img_stem}_${stat_thresh}_stats.nii.gz
}
#==============================================================================
: <<COMMENTBLOCK
Function: GetLat
Purpose: 	Extract the laterality info for each region
Calls: DefMeasures and LatOut
COMMENTBLOCK

GetLat() {
  DefVarsLat
  CleanLat
  # Run processing separately on the left and right for the laterality index
  echo "Retrieving count of above threshold voxels in each region on the left."
  echo "-------------------------------------------------------------"
  for roi in ${SCIF_APPBIN}/ATLASES/${atlas}/*_L.nii.gz; do
    DefMeasuresLat
    echo "Suprathreshold voxel count in left ${roi_base_LI_L}: ${VoxSuprathresh_L}"
    echo "-------------------------------------------------------------"
    echo "${roi_base_LI_L},${VoxSuprathresh_L},${img_stem}_${stat_thresh}" >>${OUTFILE_LI_L}
  done

  echo "============================================================="
  echo "Retrieving count of above threshold voxels in each region on the right."
  echo "-------------------------------------------------------------"
  for roi in ${SCIF_APPBIN}/ATLASES/${atlas}/*_R.nii.gz; do
    DefMeasuresLat
    echo "Suprathreshold voxel count in right ${roi_base_LI_R}: ${VoxSuprathresh_R}"
    echo "-------------------------------------------------------------"
    echo "${roi_base_LI_R},${VoxSuprathresh_R},${img_stem}_${stat_thresh}" >>${OUTFILE_LI_R}
  done
}

#==============================================================================
: <<COMMENTBLOCK
Function: GetMask
Purpose: 	Extract the laterality info for each region
Calls: DefVarsMask
COMMENTBLOCK

GetMask() {
  DefVarsMask
  CleanMask
  # Run processing separately for mask
  echo "Retrieving count of above threshold voxels in each region."
  echo "-------------------------------------------------------------"

  for roi in ${SCIF_APPBIN}/ATLASES/${atlas}/*.nii.gz; do
    DefMeasuresMask
    good=$(echo "${MaskVol} > ${stat_thresh}" | bc)
    if [[ ${good} -eq 1 ]]; then
      echo "Mask volume in ${roi_base}: ${MaskVol} mm3 out of ${RoiVol} mm3: percent: ${percent_mask}"
      echo "-------------------------------------------------------------"
      echo "${roi_base},${RoiVol},${MaskVol},${percent_mask},${img_stem}_mask" >>${OUTFILE_MASK1}
    fi
  done
}
#==============================================================================
: <<COMMENTBLOCK
Function: GetStats
Purpose: 	Extract the NIfTI and Statistical CSV for regions above threshold
Calls: DefMeasures and StatsOut
COMMENTBLOCK

GetStats() {
  echo "Creating NIfTI and Statistical CSV for regions above threshold."
  echo "-------------------------------------------------------------"
  DefVarsStats
  CleanStats
  for roi in ${SCIF_APPBIN}/ATLASES/${atlas}/*.nii.gz; do
    DefMeasuresStats
    # MeanStat is probably a float, so use bc to determine whether
    # it is above or below the threshold. This particular
    # use of bc returns a boolean: 1=yes, 0=no
    good=$(echo "${MeanStat} >= ${stat_thresh}" | bc)
    # If the roi meets the criterion threshold, add it to the output file
    if [[ ${good} -eq 1 ]]; then
      echo "${roi_base},${MeanStat},${PeakStat},${PeakX},${PeakY},${PeakZ},${SDStat},${img_stem}_${stat_thresh}" >>${OUTFILE1}
      echo "${roi_base} suprathreshold mean: ${MeanStat}"
      echo "-------------------------------------------------------------"
      if [ ! -e ${OUTIMG} ]; then
        imcp ${roi} ${OUTIMG}
        # subtract the outimage from itself to get an empty image.
        fslmaths ${OUTIMG} -sub ${OUTIMG} ${OUTIMG} -odt short
      fi
      # Create an roi with the correct internal value
      fslmaths ${roi} -mul ${MeanStat} ${SCIF_APPBIN}/${roi_base}_val
      # Add these rois with values to the outimg
      fslmaths ${OUTIMG} -add ${SCIF_APPBIN}/${roi_base}_val ${OUTIMG}
      imrm ${SCIF_APPBIN}/${roi_base}_val
    fi
  done

  # Make sure fsleyes recognizes the output NIfTI image is in MNI152 space.
  fslcpgeom ${SHARED}/MNI152_T1_2mm ${OUTIMG}
}
#==============================================================================
: <<COMMENTBLOCK
Function: LatOut
Purpose: 	Write the csv output files for the laterality measures
COMMENTBLOCK

LatOut() {
  # Join the data generated by the left and right laterality counts, do some calculations
  # and join with existing info stored in the ATLASES diretory inside the container.

  join -t, ${OUTFILE_LI_L} ${OUTFILE_LI_R} | awk -F "," '{ strength= $2 + $4; laterality= $2 - $4; LI= laterality / strength; print $1","$2","$4","strength","laterality","LI","$5}' | sed 's/-nan/0/g' >>${OUTFILE_LI_TEMP}

  rm ${OUTFILE_LI_L} ${OUTFILE_LI_R}

  if [ "${atlas}" = "HCP-MMP1" ]; then
    echo "Region,VoxSuprathresh_L,VoxSuprathresh_R,Strength,Laterality,LI,Lobe,RegionLongName,Cortex,Image" >>${OUTFILE_LI}
    # Join lobe and regionLongName information to the laterality index CSV
    join -t, ${OUTFILE_LI_TEMP} ${LOBE_LIST} | tr -d '\r' | awk -F "," '{print $1","$2","$3","$4","$5","$6","$8","$9","$10","$7}' >>${OUTFILE_LI}
  else
    echo "Region,VoxSuprathresh_L,VoxSuprathresh_R,Strength,Laterality,LI,Lobe,RegionLongName,Image" >>${OUTFILE_LI}
    # Join lobe and regionLongName information to the laterality index CSV
    join -t, ${OUTFILE_LI_TEMP} ${LOBE_LIST} | tr -d '\r' | awk -F "," '{print $1","$2","$3","$4","$5","$6","$8","$9","$7}' >>${OUTFILE_LI}
  fi

  rm ${OUTFILE_LI_TEMP}

  echo "============================================================="
  echo "Output Laterality Index file is named"
  echo "${atlas}_${img_stem}_${stat_thresh}_LI.csv"
  echo "============================================================="
}

#==============================================================================
: <<COMMENTBLOCK
Function: MaskOut
Purpose: 	Write the csv output files for the laterality measures
COMMENTBLOCK

MaskOut() {

  # Join the data from the generated CSV with existing info stored in the ATLASES
  # directory in the container.  This provides more complete and useful CSV files.
  echo "OUTFILE_MASK1=${OUTFILE_MASK1} ATLAS_LIST=${ATLAS_LIST}"
  if [ "${atlas}" = "HCP-MMP1" ]; then
    echo "Region,LR,Lobe,RegionLongName,Cortex,RegionVol_mm3,MaskVol_in_Region_mm3,MaskVol_in_Region_Percent,Image" >>${OUTFILE_MASK2}
    join -t, ${OUTFILE_MASK1} ${ATLAS_LIST} | sed 's/ //g' | tr -d '\r' | awk -F "," '{print $1","$6","$7","$11","$13","$2","$3","$4","$5}' >>${OUTFILE_MASK2}
  else 
    echo "Region,LR,Lobe,RegionLongName,RegionVol_mm3,MaskVol_in_Region_mm3,MaskVol_in_Region_Percent,Image" >>${OUTFILE_MASK2}
    join -t, ${OUTFILE_MASK1} ${ATLAS_LIST} | sed 's/ //g' | tr -d '\r' | awk -F "," '{print $1","$6","$7","$11","$2","$3","$4","$5}' >>${OUTFILE_MASK2}
  fi

  rm ${OUTFILE_MASK1}

  echo "============================================================="
  echo "Output CSV file is named"
  echo "${atlas}_${img_stem}_mask.csv"
  echo "============================================================="
}

#==============================================================================
: <<COMMENTBLOCK
Function: StatsOut
Purpose: 	Write the csv output files for the stats
COMMENTBLOCK

StatsOut() {
  # Join the data from the generated CSV with existing info stored in the ATLASES
  # directory in the container.  This provides more complete and useful CSV files.
  echo "OUTFILE1=${OUTFILE1} ATLAS_LIST=${ATLAS_LIST}"
  if [ "${atlas}" = "HCP-MMP1" ]; then
    echo "Region,Mean,Peak,PeakX,PeakY,PeakZ,SD,LR,Lobe,CogX,CogY,CogZ,RegionLongName,RegionVol_MM3,Cortex,Image" >>${OUTFILE2}
    join -t, ${OUTFILE1} ${ATLAS_LIST} | sed 's/ //g' | tr -d '\r' | awk -F "," '{print $1","$2","$3","$4","$5","$6","$7","$9","$10","$11","$12","$13","$14","$15","$16","$8}' >>${OUTFILE2}
  else 
    echo "Region,Mean,Peak,PeakX,PeakY,PeakZ,SD,LR,Lobe,CogX,CogY,CogZ,RegionLongName,RegionVol_MM3,Image" >>${OUTFILE2}
    join -t, ${OUTFILE1} ${ATLAS_LIST} | sed 's/ //g' | tr -d '\r' | awk -F "," '{print $1","$2","$3","$4","$5","$6","$7","$9","$10","$11","$12","$13","$14","$15","$8}' >>${OUTFILE2}
  fi

 rm ${OUTFILE1}

  echo "============================================================="
  echo "Output NIfTI and CSV files are named"
  echo "${atlas}_${img_stem}_${stat_thresh}_stats.csv"
  echo "and"
  echo "${atlas}_${img_stem}_${stat_thresh}_stats.nii.gz" 
  echo "============================================================="
}
#==============================================================================
: <<COMMENTBLOCK
Function: HelpMessage
Purpose: 	Provide help if there are no arguments
COMMENTBLOCK

HelpMessage() {
  echo "Purpose: Make a list of regions with activation"
  echo "   		   at a given statistical-threshold"
  echo "Input:   1) The function to run: Stats, Lat or Mask. 
                 Stats generates mean and peak values for above threshold voxels in a stats image
                 Lat generates laterality info for above threshold values in a stats image
                 Mask will generate percent_masked values for each region.  It works with a binary mask in standard space.
                 2) Standard space stats image"
  echo "         3) A stat_threshold should be passed in to limit the output"
  echo "            Keep in mind that values could be negative"
  echo "         4) Atlas (currently HO-CB, HCP-MMP1, ARTERIAL1, ARTERIAL1_GM-WM, ARTERIAL2," 
  echo "         Schaefer2018_100Parcels_Kong2022_17Networks, Schaefer2018_100Parcels_17Networks )"
  echo "                                                           "
  echo "         $0 xtract Stats run-01_IC-06_Russian_Unlearnable.nii 1.3 HO-CB"
  echo "         The underlying docker command that is generated is:"
  echo "         e.g., docker run -it --rm -v ${PWD}:/scif/data/roixtractor
                 diannepat/scif_fsl run roixtractor Stats run-01_IC-06_Russian_Unlearnable.nii 1.3 HO-CB"
  echo "Output:  If you run Stats, a CSV file containing Mean & Peak vals for each region that meets the criterion:"
  echo "         e.g., HO-CB_run-01_IC-06_Russian_Unlearnable_1.3.csv"
  echo "         A NIfTI file, containing the mean value for each region that meets criterion:"
  echo "         e.g., HO-CB_run-01_IC-06_Russian_Unlearnable_1.3.nii.gz"
  echo ""
  echo "Full Help: sfw.sh help roixtractor"
  exit 1
}

#==============================================================================
if [ $# -lt 1 ]; then
  HelpMessage
else
  func=$1
  echo ${func}
  img_stem=$(remove_ext "$2")
  img_loc=${SCIF_APPDATA}/${img_stem}
  stat_thresh=${3}
  atlas=${4}
  ATLAS_LIST=${SCIF_APPBIN}/ATLASES/${atlas}_List.csv
  LOBE_LIST=${SCIF_APPBIN}/ATLASES/${atlas}_Lobes_List.csv
fi

if [ ${func} = Stats ]; then
  CheckDim
  GetStats
  StatsOut
elif [ ${func} = Lat ]; then
  CheckDim
  GetLat
  LatOut
elif [ ${func} = Mask ]; then
  stat_thresh=0
  CheckDim
  # fslmaths ${img_loc} ${img_loc} -odt char
  GetMask
  MaskOut
fi
