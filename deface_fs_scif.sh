#!/bin/bash

if [ $# -lt 1 ]
then
    echo "Usage: $0 <input file>"
    echo "Example: $0 mprage.nii.gz"
    echo "defaces a structural image, creating img_defaced.nii.gz"
    echo "prefers T1 images"
    exit 1
fi

img=${1}
img_stem=`remove_ext ${1}`

# Ensure orientation is correct, as this helps deface.
echo "reorienteing ${img_stem}"
fslreorient2std ${SCIF_APPDATA}/${img_stem} ${SCIF_APPDATA}/${img_stem}

echo "defacing ${img}"
if [ ! -e ${SCIF_APPDATA}/${img_stem}_defaced.nii.gz ]; then
  mri_deface ${SCIF_APPDATA}/${img} ${SHARED}/talairach_mixed_with_skull.gca ${SHARED}/face.gca ${SCIF_APPDATA}/${img_stem}_defaced.nii

  gzip ${SCIF_APPDATA}/${img_stem}_defaced.nii
fi

fslmaths ${SCIF_APPDATA}/${img_stem}_defaced.nii.gz ${SCIF_APPDATA}/${img_stem}_defaced.nii.gz -odt short

# Generate a difference mask image.
if [ ! -e ${SCIF_APPDATA}/${img_stem}_deface_mask.nii.gz ]; then
  fslmaths ${SCIF_APPDATA}/${img_stem}_defaced -bin ${SCIF_APPDATA}/${img_stem}_defacemask -odt char
fi
echo "=================================================================="
echo "You should check your defacing results.  The mask should cover the entire brain:"
echo ">fsleyes ${img_stem} ${img_stem}_defacemask -cm Red -a 40"
echo "If the mask does not entirely overlap the brain, manually add voxels, then,"
echo ">fslmaths ${img_stem} -mul ${img_stem}_defacemask ${img_stem}_defaced"
