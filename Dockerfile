FROM diannepat/fsl5-core

# For testing, don't run the update
# RUN apt-get update \
#     rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Set up the deface_fs app
ADD deface_fs.scif /
RUN scif install /deface_fs.scif
ADD deface_fs_scif.sh /scif/apps/deface_fs/bin

# Set up the roixtractor app
ADD roixtractor.scif /
# set up the app directory structure before adding stuff to it
RUN scif install /roixtractor.scif
# add stuff from local directory context to the container
ADD xtractor_scif.sh /scif/apps/roixtractor/bin
ADD ATLASES /scif/apps/roixtractor/bin/ATLASES
ENTRYPOINT ["scif"]
