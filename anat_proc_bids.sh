#!/bin/bash
#==============================================================================

: <<COMMENTBLOCK

Function:   anat_proc
Purpose:    crop and bias correct image, handles lesion mask
Input:      T1w image in ${bids_dir}/anat
Output:     A cropped and bias corrected T1w image in ${bids_dir}/anat.
            The original T1w image is moved to sourcedata/${subj}/anat

COMMENTBLOCK

anat_proc ()
{
echo "------------------------------------------------"
echo "Run fsl_anat_alt with optiBET"
#if [ ! -e ${output_dir}/${subj}/anat/${subj}_defacemask.nii.gz ]; then
  # Ensure orientation is correct, as this helps deface.
  # fslreorient2std ${bids_dir}/${subj}/anat/${subj}_T1w.nii.gz
  # Reorient, crop, bias correct and mask before defacing.
  # If the lesion mask exists, then process it too.
  if [ -e ${bids_dir}/${subj}/anat/${subj}_label-lesion_roi.nii.gz ]; then
    echo "There is a lesion mask."
    fsl_anat_alt -i ${bids_dir}/${subj}/anat/${subj}_T1w.nii.gz -m ${bids_dir}/${subj}/anat/${subj}_label-lesion_roi.nii.gz --noseg --nosubcortseg
    echo "Move original lesion mask for safe keeping!!!! The new lesion-mask is adjusted for the cropped image"
    mv ${bids_dir}/${subj}/anat/${subj}_label-lesion_roi.nii.gz ${bids_dir}/sourcedata/${subj}/anat/
  else
    echo "There is NO lesion mask."
    fsl_anat_alt -i ${bids_dir}/${subj}/anat/${subj}_T1w.nii.gz --noseg --nosubcortseg
  fi

  echo "fsl_anat_alt has finished"
  echo "------------------------------------------------"
  echo "Move the original image for safe keeping"
  mv ${bids_dir}/${subj}/anat/${subj}_T1w.nii.gz ${bids_dir}/sourcedata/${subj}/anat/${subj}_T1w.nii.gz

  echo "Replace original Tw image with cropped bias-corrected image"
  mv ${bids_dir}/${subj}/anat/${subj}_T1w.anat/T1_biascorr.nii.gz ${bids_dir}/${subj}/anat/${subj}_T1w.nii.gz

  echo "Move the brain_mask to the output dir"
  mv ${bids_dir}/${subj}/anat/${subj}_T1w.anat/T1_biascorr_brain_mask.nii.gz ${output_dir}/${subj}/anat/${subj}_T1w_mask.nii.gz

  echo "remove identifiable images from anat subdirectory"
  imrm ${bids_dir}/${subj}/anat/${subj}_T1w.anat/T1
  imrm ${bids_dir}/${subj}/anat/${subj}_T1w.anat/T1_orig
  imrm ${bids_dir}/${subj}/anat/${subj}_T1w.anat/T1_fullfov
  echo "move the anat subdir to the output directory"
  mv ${bids_dir}/${subj}/anat/${subj}_T1w.anat ${output_dir}/${subj}/anat/
  # echo "segmentation will need an image named T1.nii.gz (though it doesn't claim to)"
  cd ${output_dir}/${subj}/anat/${subj}_T1w.anat
  # it'd be nice to use a symlink here, but globus ignores symlinks, causing a world of hurt on the other end.  Better to use the space ; (

  echo "cp T1_biascorr to T1"
  cp T1_biascorr.nii.gz T1.nii.gz
  else
    echo "${subj}_defacemask.nii.gz exists, so fsl_anat_alt and defacing were not run."
#fi

}

#==============================================================================
: <<COMMENTBLOCK
Create an output directory under derivatives: anat_proc
COMMENTBLOCK

# We pass 3 arguments from run.py: the subjectnum, bids_dir and output_dir

subjectnum=$1
subj=sub-${subjectnum}
bids_dir=$2
output_dir=$3
###################
echo "subject=${subj}"
echo "bids_dir = ${bids_dir}"
echo "output_dir = ${output_dir}"

if [ $# -lt 1 ]
    then
        HelpMessage
        exit 1
fi

Main ${subjectnum} ${bids_dir} ${output_dir}
#==============================================================================
